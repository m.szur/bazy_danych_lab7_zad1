const express = require("express");
const recordRoutes = express.Router();
const dbo = require("../db/conn");
const ObjectId = require("mongodb").ObjectId;

// 3. zwracanie produktów na /products

recordRoutes.route("/products").get(function(req, res) {
    let db_connect = dbo.getDb("test_example");

    const {name, price, quantity, sortBy, sortOrder} = req.query
    const filterOptions = {};

    if (name) filterOptions.name = new RegExp(name, 'i');
    if (price) filterOptions.price = parseFloat(price);
    if (quantity) filterOptions.quantity = parseInt(quantity);

    const sortOptions = {};

    if (sortBy) sortOptions[sortBy] = sortOrder === 'desc' ? -1 : 1;

    console.log('Filter:', filterOptions);
    console.log('Sort:', sortOptions);

    db_connect.collection("products").find(filterOptions).sort(sortOptions).toArray(function(err, result) {
        if (err) throw err;
        res.json(result);
    });
});

// 4. dodawanie nowego produktu na /products

recordRoutes.route("/products").post(function(req, res){
    let db_connect = dbo.getDb("test_example");
    const { name, price, description, quantity, unit } = req.body

    if (!name) {
        return res.status(400).json({ error: 'Product name is required.' });
    }

    db_connect.collection("products").findOne({ name: name }, function(err, existingProduct) {
        if (err) {
            console.error('Error:', err);
            return res.status(500).json({ error: 'Internal Server Error' });
        }

        if (existingProduct) {
            // nazwa produktu już istnieje
            return res.status(400).json({ error: 'Product with the same name already exists.' });
        }

        const newProduct = { name, price, description, quantity, unit };

        db_connect.collection("products").insertOne(newProduct, function(err, result) {
            if (err) throw err;
            return res.status(201).json({ message: 'Product added successfully.', productId: result.insertedId });
        });
        
    });
});

// 5. zmiana danych w istniejącym produkcie na /products/:id 

recordRoutes.route("/products/:id").put(function(req, response){
    let db_connect = dbo.getDb("test_example");
    let myquery = {_id: ObjectId(req.params.id)};
    let newValues = {
        $set: {
            name: req.body.name,
            price: req.body.price,
            description: req.body.description,
            quantity: req.body.quantity,
            unit: req.body.unit
        },
    };
    db_connect.collection("products").updateOne(myquery, newValues, function(err, res){
        if (err) throw err;
        if (res.matchedCount === 0) {
            // brak produktu w bazie danych
            return response.status(404).json({ error: 'Product not found' });
        }

        console.log("1 document updated successfully");
        response.json({ message: 'Product updated successfully' });
    });
});

// 6. usuwanie istniejącego produktu na /products/:id

recordRoutes.route("/products/:id").delete(function (req, res) {
    let db_connect = dbo.getDb("test_example");
    let myquery = { _id: ObjectId(req.params.id) };

    db_connect.collection("products").deleteOne(myquery, function (error, result) {
        if (error) {
            console.error('Error:', error);
            return res.status(500).json({ error: 'Internal Server Error' });
        }

        if (result.deletedCount === 1) {
            console.log("1 document deleted");
            return res.json({ message: "Product deleted successfully" });
        } else {
            return res.status(404).json({ error: "Product not found" });
        }
    });
});

//  7. raportowanie stanu magazynu na /reports

recordRoutes.route("/reports").get(function(req, res) {
    let db_connect = dbo.getDb("test_example");

    // ilosc produktow
    const quantityReportPipeline = [
        {
            $group: {
                _id: "$name",
                totalQuantity: { $sum: "$quantity" }
            }
        }
    ];

    // łączna wartosc produktow
    const valueReportPipeline = [
        {
            $group: {
                _id: null,
                totalValue: { $sum: { $multiply: ["$price", "$quantity"] } }
            }
        }
    ];

    Promise.all([
        db_connect.collection("products").aggregate(quantityReportPipeline).toArray(),
        db_connect.collection("products").aggregate(valueReportPipeline).toArray()
    ])
    .then(([quantityReport, valueReport]) => {
        const report = {
            quantityReport,
            valueReport: valueReport.length > 0 ? valueReport[0].totalValue : 0
        };
        res.json(report);
    })
    .catch(err => {
        console.error('Error:', err);
        res.status(500).json({ error: 'Internal Server Error' });
    });
});


module.exports = recordRoutes;