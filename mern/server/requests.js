const axios = require("axios");

// 3. test bez filter i bez sort

// axios.get('http://127.0.0.1:5000/products')
//   .then(response => console.log(response.data))
//   .catch(error => console.error('Error:', error));

// 3. test z filter

// axios.get('http://127.0.0.1:5000/products', {
//   params: {
//     name: "Bread"
//   }
// })
//   .then(response => console.log(response.data))
//   .catch(error => console.error('Error:', error));

// 3. test z sort

// axios.get('http://127.0.0.1:5000/products', {
//   params: {
//     sortBy: "price",
//     sortOrder: "desc"
//   }
// })
//   .then(response => console.log(response.data))
//   .catch(error => console.error('Error:', error));


// 4. test 

// axios.post('http://127.0.0.1:5000/products', {
//   "name": "Chocolate Covered Strawberries ",
//   "price": 0.60,
//   "description": "A brand new product",
//   "quantity": 50,
//   "unit": "units"
// })
//   .then(response => console.log(response.data))
//   .catch(error => console.error('Error:', error));

// 5. test

// axios.put('http://127.0.0.1:5000/products/6559fad9655bc423d49fd3c2', {
//   name: "Bread",
//   price: 2.5,
//   description: "Freshly baked bread",
//   quantity: 100,
//   unit: "pieces"
// })
//   .then(response => console.log(response.data))
//   .catch(error => console.error('Error:', error));

// 6. test

// axios.delete('http://127.0.0.1:5000/products/655a2767eecece8395e4f213')
//   .then(response => console.log(response.data))
//   .catch(error => console.error('Error:', error));

// 7. test

axios.get('http://127.0.0.1:5000/reports')
  .then(response => console.log(response.data))
  .catch(error => console.error('Error:', error));
